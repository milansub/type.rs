#!/bin/bash

sudo which docker >/dev/null && sudo docker --version | grep "Docker version" > /dev/null

if [ $? -eq 0 ]
then
	echo "You have docker installed."
else
	sudo apt-get update && sudo apt-get install docker.io 
fi

if pgrep -f docker > /dev/null ;
then 
	echo "Docker is already running."
else
	sudo service docker start
	echo "Docker started."
fi
